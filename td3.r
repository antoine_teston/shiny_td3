#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#
# ----------------------------------------------------------------------------

# Load packages
# ----------------------------------------------------------------------------
library(shiny)
library(dplyr)
library(lubridate)

library(DBI)
library(RPostgres)


# Load connection parameters
# ----------------------------------------------------------------------------
source("connection.R")


# Load data
# ----------------------------------------------------------------------------

con <- dbConnect(RPostgres::Postgres(), 
                 dbname = db, 
                 host=host_db, 
                 port=db_port, 
                 user=db_user, 
                 password=db_password)

hospital_stay = DBI::dbGetQuery(
    con, 
    "select * from sejour")

str(hospital_stay)

hospital = DBI::dbGetQuery(
    con, 
    "select * from hopital")

str(hospital)


# Data management
# ----------------------------------------------------------------------------

hospital_stay = hospital_stay %>%
    rename(admission_date = date_debut_sejour,
           discharge_date = date_fin_sejour,
           hospital_id = id_hopital)

hospital = hospital %>%
    rename(hospital = hopital,
           hospital_id = id_hopital)

# User Interface
# ----------------------------------------------------------------------------

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("TD 3 - Connection to a database"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            selectInput(
                inputId= "select_hospital", "Unit",
                choices= c("All", hospital$hospital)
            )
        ),

        # Show a plot of the generated distribution
        mainPanel(
            tableOutput("selected_hospital_stay")
        )
    )
)

# Server
# ----------------------------------------------------------------------------

# Define server logic required to draw a histogram
server <- function(input, output) {

    output$selected_hospital_stay <- renderTable({
        
        p = hospital_stay %>% 
            merge(hospital, id = "hospital_id") %>%
            filter(hospital == input$select_hospital) %>%
            select(hospital_id, admission_date, discharge_date, hospital)
        print(p)
        return(p)

    })
}


# Web application
# ----------------------------------------------------------------------------

# Run the application 
shinyApp(ui = ui, server = server)
